One or few click deletion of all conversations (iMessages and SMS texts) in Message on Mac OS X.

Download and place in $HOME/Library/Scripts/

There are several versions due to Apple deleting Applescript capabilities from Messages over the years.

* For OS X before Big Sur, use "Delete All Messages".

* For Big Sur, use "Delete All Messages (Big Sur)". In order to use this script, first open Settings->Keyboard->Shortcuts and check "Use keyboard navigation to move focus between controls".

* For Ventura, use "Delete 10 Messages (Ventura)".  You can edit it to change the number of messages deleted.  The number is fixed because you can no longer get an accurate count of the number of conversations in Messages with Applescript.  It does not require setting keyboard shortcuts.

